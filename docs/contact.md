# Contact Us
You may provide feedback on the code of conduct or submit questions in the following ways:
 

- Email the secretariat directly at [secretariat@openteam.community](mailto:secretariat@openteam.community)

- Submit a response via the [feedback forum](https://docs.google.com/forms/d/e/1FAIpQLSfbUqXnjDnl4w7qZRKzc62JQbllmJJDR2X4tVdWtiyuyFhV7w/viewform?usp=sf_link)
