# OpenTEAM Principles & Values

## OpenTEAM **Principles**
- Agriculture is a shared human endeavor and we have a shared interest in providing everyone the best possible, site-specific, and  data-driven agricultural knowledge.
- Agriculture is a public science, where we will collectively benefit by creating a pre-competitive space to more rapidly advance our shared understanding of food systems, agroecosystems, climate science, and relationships between soil health and human health. 
- A shared urgency exists in taking action to improve soil health globally and support continual improvement of ecosystem service delivery from farm and ranchland.
- The majority of benefit and natural wealth creation should flow to those most directly engaged in the transformation of natural systems.   
- Knowledge is co-created, and user-generated data should be shared as opt-in and controlled by those who created it.  
- Pre-competitive contributions to OpenTEAM from each member will help pool knowledge, resources and capabilities in order to speed up the systemic challenges of farmer resiliency and ag data interoperability we seek to solve.

## OpenTEAM has identified and seeks to hold these **values** as it works with its membership to build out its community and ecosystem:
- Gratitude and Trust
- Shared, positive learning environment
- Context & Reflection
- Bridging affinities
- Community approach to tackling challenges impossible to tackle alone
- Responsiveness to the dynamic space of agriculture, climate and technology
- Representative governance that preserves data sovereignty and equity which in-turn are important to build and maintain trust necessary for a functional community (research or otherwise)
