# OpenTEAM Code of Conduct
*Drafted and approved by OpenTEAM Secretariat, 10/13/2020*

Open Technology Ecosystem for Agricultural Management (OpenTEAM) is a collaborative community of farmers, scientists and researchers, engineers, farm service providers, and food companies that are committed to improving soil health and advancing agriculture’s ability to become a solution to climate change.
This code of conduct applies to all OpenTEAM community spaces, both online and in person.
While we operate under the assumption that all people involved with OpenTEAM subscribe to the shared principles of OpenTEAM, we take Code of Conduct violations very seriously. Therefore, violations of this Code may affect an individual’s ability to participate in OpenTEAM, ranging from temporarily being placed into online moderation to, as a last resort, expulsion from the community or in-person events. If you have any questions about our commitment to this framework and/or if you are unsure about any aspects of it, email secretariat@openteam.community and we will provide clarification.
In order to uphold [the principles](https://openteamag.gitlab.io/codesign/code-of-conduct/Principlesandvalues/) we support as an OpenTEAM we agree to the following code of conduct:

### Our Pledge

We, as OpenTEAM members, contributors, and leaders, pledge to make participation in our
community a harassment-free experience for everyone, regardless of age, body size, visible or invisible disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, or sexual identity and orientation.

We pledge to act and interact in ways that contribute to an open, welcoming, diverse, inclusive and healthy community.

## How It Works
This Code is an effort to maintain a respectful space for everyone and to discuss what might happen if that space is compromised. 
### We listen.
We begin interactions by acknowledging that we are part of a community with complementary goals. When something has happened and someone is uncomfortable, our first choice is to work through it through discussion. We listen to each other.
For active listening, we ask questions first, instead of making statements.
We give people time and space to respond.
We appropriately adjust our behavior when asked to.
We know that repeating hurtful behavior after it has been addressed is disrespectful.
We avoid this ourselves and help others identify when they are doing it.
Examples of behavior that contributes to a positive environment for our
community include:

* Demonstrating empathy and kindness toward other people
* Being respectful of differing opinions, viewpoints, and experiences
* Giving and gracefully accepting constructive feedback
* Accepting responsibility and apologizing to those affected by our mistakes, and learning from the experience
* Focusing on what is best not just for us as individuals, but for the overall community

### Examples of behavior that contributes to a positive environment for our
community include:

* Demonstrating empathy and kindness toward other people
* Being respectful of differing opinions, viewpoints, and experiences
* Giving and gracefully accepting constructive feedback
* Accepting responsibility and apologizing to those affected by our mistakes, and learning from the experience
* Focusing on what is best not just for us as individuals, but for the overall community


### Examples of unacceptable behavior include:

* The use of sexualized language or imagery, and sexual attention or advances of any kind
* Trolling, insulting or derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or email address, without their explicit permission
* Other conduct which could reasonably be considered inappropriate in a professional setting


### Online Community Behavior Guidance

| Do                                                                                                                                  | Don’t                                                                                                                                                                                                      |
| ----------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Stay on topic** to make long threads easier to follow.                                                                                | **Do not send unnecessary one-line responses** that effectively “spam” hundreds of people and lower the overall content quality of a conversation. (Exception: expressions of appreciation and encouragement!) |
| **Start a new thread to help others follow along.** Important if your response starts to significantly diverge from the original topic. | Do not respond with off-topic information, making it hard for the large group of readers to follow along.                                                                                                  |
| **Write short and literal subject lines** to help the readers of the list manage the volume of communication.                           | Humor and euphemisms in subject lines are easily misunderstood, although enthusiasm is welcome!                                                                                                            |
| **Mind your tone.** We are not having this conversation in person, so it is all the more important to maintain a tone of respect.       | Do not write in an aggressive, disrespectful or mocking tone. Note: writing in all caps is regarded as shouting.                                                                                           |

### In-person Community Behavior Guidance

| Do                                                                                                                                                                                                                    | Don’t                                                                                                                                                    |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Ask permission to take pictures of and post about others on social media (see Media Consent, below).                                                                                                                  | Do not upload photos, tag or mention others online without their consent.                                                                                |
| Speak your own narrative, from your own unique experiences and culture.                                                                                                                                               | Do not imitate the cultural expressions of groups you are not a member of, or dismiss people’s experiences as illegitimate or merely personal.           |
| Use accessible language to talk about your area of expertise. If others in the group seem confused, slow down; stop and ask for input.                                                                                | Do not present information in a way / language that no one else in the room can understand, with no attempt to include others in the discussion.         |
| Give everyone a chance to talk, only interrupting if absolutely necessary – for example, for Code of Conduct violations or time updates.                                                                              | Do not repeatedly disrupt a discussion.                                                                                                                  |
| Stop, listen and ask for clarification if someone perceives your behavior or presentation as violating the Code of Conduct.                                                                                           | Do not ignore or argue others’ request to stop potentially harmful behavior, even if it was an accident or you don’t mean it as it is being interpreted. |
| Use words that accurately describe the situation rather than culturally or socially loaded terms – For example, “The wind was ridiculously strong!” instead of “The wind was crazy!”                                  | Do not use disability and mental/emotional health terminology to describe a situation metaphorically, even if it seems normal to use it.                 |
| Ask someone before you touch them, even when joking or greeting, unless the other person has given verbal consent. Hugs, cheek kisses, and handshakes are normal greetings in some cultures, but not in all cultures. | Do not initiate or simulate physical contact without consent, even if it seems normal.                                                                   |
| Disengage and find another activity if someone did not invite you and is not engaging with you.                                                                                                                       | Do not violate personal space by continuing your physical presence into private spaces without consent.                                                  |
| Use an even tone, rate, and volume of voice when disagreeing.  Note that differences will be common, and some will be irreconcilable in a diverse movement.                                                           | Do not verbally or physically abuse, harass, yell at, or intimidate any attendee, speaker, volunteer, or exhibitor.                                      |
| Use the pronouns people have specified for themselves.                                                                                                                                                                | Do not purposely misgender someone (ie, refusing to use their correct gender pronouns) after they have told you their correct pronouns.                  |
| Step up and comment when you see violations occur by emailing secretariat@openteam.community                                                                                                                          | Do not expect that people who are subject to Code of Conduct violations are comfortable or able to address or report them themselves.                    |

## Enforcement Responsibilities

The Secretariat is responsible for clarifying and enforcing our standards of acceptable behavior and will take appropriate and fair corrective action in response to any behavior that they deem inappropriate, threatening, offensive, or harmful.

The Secretariat has the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct, and will communicate reasons for moderation decisions when appropriate.

**Scope**

This Code of Conduct applies within all community spaces, and also applies when an individual is officially representing the community in public spaces. Examples of representing our community include using an official e-mail address, posting via an official social media account, or acting as an appointed representative at an online or offline event.

**Enforcement**

Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by emailing secretariat@openteam.community. To report an issue involving her/him, please email sconnolly@wolfesneck.org.

The Secretariat is obligated to respect the privacy and security of the reporter of any incident.

**Enforcement Guidelines**

Community leaders will follow these Community Impact Guidelines in determining
the consequences for any action they deem in violation of this Code of Conduct:

1. Correction

**Community Impact**: Use of inappropriate language or other behavior deemed
unprofessional or unwelcome in the community.

**Consequence**: A private, written warning from community leaders, providing
clarity around the nature of the violation and an explanation of why the
behavior was inappropriate. A public apology may be requested.

2. Warning

**Community Impact**: A violation through a single incident or series
of actions.

**Consequence**: A warning with consequences for continued behavior. No
interaction with the people involved, including unsolicited interaction with
those enforcing the Code of Conduct, for a specified period of time. This
includes avoiding interactions in community spaces as well as external channels
like social media. Violating these terms may lead to a temporary or
permanent ban.

3. Temporary Ban

**Community Impact**: A serious violation of community standards, including
sustained inappropriate behavior.

**Consequence**: A temporary ban from any sort of interaction or public
communication with the community for a specified period of time. No public or
private interaction with the people involved, including unsolicited interaction
with those enforcing the Code of Conduct, is allowed during this period.
Violating these terms may lead to a permanent ban.

4. Permanent Ban

**Community Impact**: Demonstrating a pattern of violation of community
standards, including sustained inappropriate behavior,  harassment of an
individual, or aggression toward or disparagement of classes of individuals.

**Consequence**: A permanent ban from any sort of public interaction within
the community.


**Online Enforcement Guidelines**
In addition to the Code, which remains in play in online spaces, our community has created specific guidelines for online interactions. If someone violates these guidelines, someone from the Secretariat will place them into moderation by changing that person’s posting permission on the relevant list or forum, on the website, or both. Our triple notification standard for moderation means a point person from the Secretariat will 1) email the person directly with a brief explanation of what was violated, 2) send a summary email to the rest of the moderators group, 3) if it happened on a public list (vs a website), notify the list that one of our members has been placed into moderation with a brief explanation of what is not tolerated.

If you wish to begin the process of getting out of moderation, respond to the email sent to you from the member of the Secretariat.

## Attribution
This Code of Conduct was created collaboratively and drew from other CoCs, including those by [Public Lab](https://publiclab.org/notes/Shannon/07-06-2016/public-lab-code-of-conduct), [International Congress of Marine Conservation 2016](http://conbio.org/mini-sites/imcc-2016/registration-participation/code-of-conduct/), [Mozilla, Contributor-Covenant](https://github.com/mozilla/diversity), and [TransH4CK](http://www.transhack.org/).




